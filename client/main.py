import modules.security as security
import modules.sensor as sensor
import modules.server as s
from time import sleep

def main():

    # get home IDs from config

    # for each home init fetures
    homeID = 0

    server = s.Server(homeID)

    # get sensors from config
    
    doorSensor = sensor.Sensor( 0, 'door' )

    homeSecurity = security.Security( homeID, [doorSensor] )

    devicesInNetwork = homeSecurity.getDevicesInNetwork()

    #get from server
    allowedDevices = ["cc:61:e5:5d:15:b3"]
    alarmStatus = homeSecurity.getAlarmStatus(allowedDevices)

    return

    #main loop
    while(True):
        # update settings from the server
        homeSettingsFromServer = server.getHomeSettings()
        if homeSettingsFromServer is not None :
            homeSettings = homeSettingsFromServer
        
        if homeSettings is None :
            break
        
        homeSecurity.check( homeSettings )

        #fire.check()
        
        sleep(1)

        #temporary
        break

if __name__ == '__main__':
    main()