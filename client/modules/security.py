import modules.server as server
import modules.sensor as sensor
from time import sleep

from subprocess import Popen, PIPE, STDOUT, check_output
import re

class Security :

    def __init__ ( self, homeID, sensors ) :
        self.server = server.Server(homeID)
        self.sensors = sensors
    
    def check( self, homeSettings ) :
        # check security alarm
        isAlarmOn = self.getAlarmStatus( homeSettings.allowedDevices )
        if homeSettings.securityOn and isAlarmOn :
            # send photos in another thread

            # wait time for auth
            if self.getSensorsStatus() :
                self.server.sendMessage( self.server.messages.sensorTriggered )
                # timer for alarm start
                secondsBeforeAlarm = 5 # add to config
                sleep( secondsBeforeAlarm )
                
            if self.getAlarmStatus( homeSettings.allowedDevices ) :
                self.server.sendMessage( self.server.messages.securityAlarmOn )
        
        # enable sound
        if homeSettings.alarmSoundOn or ( homeSettings.securityAlarmSoundOn and isAlarmOn ) :
            self.enableAlarmSound( True )
        else :
            self.enableAlarmSound( False )
        return

    def getAlarmStatus( self, allowedDevices ):
        devicesInNetwork = self.getDevicesInNetwork()

        for deviceMacInNetwork in devicesInNetwork :
            for allowedDeviceMac in allowedDevices :
                if deviceMacInNetwork == allowedDeviceMac :
                    # disable alarm
                    return False

        return self.getSensorsStatus()

    def getDevicesInNetwork( self ) :
        devicesInNetwork = []

        self.pingHostname("192.164.100.3")
        # clear cache ip -s -s neigh flush all
        # get network devices info
        processID = Popen(["arp", "-a"], stdout=PIPE)       
        lines = processID.stdout.readlines()
        processID.communicate()[0]
        for line in lines :
            mac = re.search( r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})", line.decode("utf-8") ).groups()[0]
            devicesInNetwork.append(mac)
        return devicesInNetwork

    def pingHostname ( self, hostname) :
        #response = os.system("ping -c 1 " + hostname)
        #output = check_output( "ping -c 1 " + hostname , stderr=STDOUT, timeout=2)
        #processID = Popen(["ping -c 1 " + hostname], stdout=PIPE)       
        # lines = processID.stdout.readlines()
        # out = processID.communicate()[0]
        return

    def getSensorsStatus( self ) :
        for sensor in self.sensors :
            if sensor.getStatus() :
                return True
        return False

    def enableAlarmSound( self, mode ) :
        return